package sva;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class MainSVA {
	
	public static void main(String[] args) {
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
        Date date = new Date();
        String currDate = formatter.format(date);
        System.out.println("Current Date: "+currDate);
		
//		String username = "ADMDBMCJ", password = "hyZm68NG2xmhfnNY";
        String username = "MSENTESRVC", password = "zLKU9pWDQs";
		System.out.println("GETTING SVA RECON REPORT, Please Wait....");
		System.out.println("");
		
		try {
			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=DriverManager.getConnection(  
					"jdbc:oracle:thin:@172.18.3.6:1521/MMPRODDB"
					+ "",username,password);
			
			System.out.println("--------------------------------------");
	        System.out.println("| Getting Records FROM CURRENTSTOCK! |");
	        System.out.println("--------------------------------------");
	        
	        String st = "SELECT DISTINCT INF.IDNUMBER, Decrypt(INF.TYPE, 'FhTQJUjwC4BmLBYwqKm', INF.MSISDN) AS TYPE,\r\n" + 
	        		"INF.LASTNAME, INF.FIRSTNAME, INF.SECONDNAME,\r\n" + 
	        		"CBALANCE.MSISDN, INF.EMAIL, INF.REGDATE, CBALANCE.UPDATEDDATE, INF.STATUS, TBALANCE FROM\r\n" + 
	        		"(\r\n" + 
	        		"   SELECT MSISDN, Decrypt(AMOUNT, 'FhTQJUjwC4BmLBYwqKm', MSISDN)/100 AS CBALANCE, WALLETID, UPDATEDDATE \r\n" + 
	        		"   FROM TBLCURRENTSTOCK WHERE WALLETID = 0 AND MSISDN > 0 \r\n" + 
	        		"   AND MSISDN IN (SELECT DISTINCT a.MSISDN FROM TBLTRANSACTIONDETAILS a)\r\n" + 
	        		") CBALANCE \r\n" + 
	        		"LEFT JOIN\r\n" + 
	        		"(SELECT MSISDN, BALANCEAFTER/100 AS TBALANCE FROM TBLTRANSACTIONDETAILS) TBALANCE \r\n" + 
	        		"ON CBALANCE.MSISDN = TBALANCE.MSISDN AND CBALANCE.CBALANCE = TBALANCE.TBALANCE\r\n" + 
	        		"INNER JOIN(\r\n" + 
	        		"    SELECT * FROM TBLMOBILEACCOUNTINFO\r\n" + 
	        		") INF ON CBALANCE.MSISDN = INF.MSISDN";
			Statement stmt=con.createStatement();  
			ResultSet rs = stmt.executeQuery(st);
			
			List<List<String>> rows = new ArrayList<>();
			while(rs.next()) {
				
//				System.out.println(rs.getString(1)+" "+rs.getString(2)+"  "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5)+" "+rs.getString(6)+" "+rs.getString(7)+" "+rs.getString(8)+" "+rs.getString(9)+" "+rs.getString(10)+" "+rs.getString(11));
				System.out.println(rs.getString(4));
				String[] ar = {rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11)};
				List<String> af = Arrays.asList(ar);
				rows.add(af);
			}
			
			String st2 = "SELECT INF.IDNUMBER, Decrypt(INF.TYPE, 'FhTQJUjwC4BmLBYwqKm', INF.MSISDN) AS TYPE,\r\n" + 
					"INF.LASTNAME, INF.FIRSTNAME, INF.SECONDNAME,\r\n" + 
					"cs.MSISDN, INF.EMAIL, INF.REGDATE, cs.UPDATEDDATE, INF.STATUS, Decrypt(cs.AMOUNT, 'FhTQJUjwC4BmLBYwqKm', cs.MSISDN)/100 AS AMOUNT FROM TBLCURRENTSTOCK cs\r\n" + 
					"INNER JOIN(\r\n" + 
					"    SELECT * FROM TBLMOBILEACCOUNTINFO\r\n" + 
					") INF ON cs.MSISDN = INF.MSISDN\r\n" + 
					"WHERE cs.WALLETID = 0\r\n" + 
					"AND cs.MSISDN NOT IN(SELECT a.MSISDN FROM TBLTRANSACTIONDETAILS a)";
			
			Statement stmt2=con.createStatement();  
			ResultSet rs2 = stmt2.executeQuery(st2);
			
			while(rs2.next()) {
				
//				System.out.println(rs2.getString(1)+" "+rs.getString(2)+"  "+rs2.getString(3)+" "+rs2.getString(4)+" "+rs2.getString(5)+" "+rs2.getString(6)+" "+rs2.getString(7)+" "+rs2.getString(8)+" "+rs2.getString(9)+" "+rs2.getString(10)+" "+rs2.getString(11));
				System.out.println(rs2.getString(4));
				String[] ar = {rs2.getString(1),rs2.getString(2),rs2.getString(3),rs2.getString(4),rs2.getString(5),rs2.getString(6),rs2.getString(7),rs2.getString(8),rs2.getString(9),rs2.getString(10),rs2.getString(11)};
				List<String> af = Arrays.asList(ar);
				rows.add(af);
			}
			
			con.close();
			System.out.println("----------------------------------------------------------------------------------");
			System.out.println(" Now Exporting 'Recon SVA Balance Detail Report ("+ currDate +").xslx' file.... |");
			System.out.println("----------------------------------------------------------------------------------");
		
			Export(rows, currDate);
			
			System.out.println("");
			System.out.println("---------------------");
			System.out.println("| Status: Finished! |");
			System.out.println("---------------------");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void Export(List<List<String>> rows, String currDate) {
		
		//Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
        
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");
        
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[] {"ACCOUNT NUMBER", "ACCOUNT TYPE", "NAME", "MOBILE NUMBER", "E-MAIL", "SIGNUP DATE", "LAST ACTIVITY DATE", "ACCOUNT STATUS", "BALANCE(UGX)"});
        int ctr = 2;
        for (List<String> rowData : rows) {
		    
        	String name = rowData.get(2)+", "+rowData.get(3)+" "+rowData.get(4);
        	
        	data.put(""+ctr, new Object[] {rowData.get(0),rowData.get(1),name,rowData.get(5),rowData.get(6),rowData.get(7),rowData.get(8),rowData.get(9),rowData.get(10)});
        	ctr++;
        }
        
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        
        try
        {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("Recon SVA Balance Detail Report ("+currDate+").xlsx"));
            workbook.write(out);
            out.close();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
	}
}
